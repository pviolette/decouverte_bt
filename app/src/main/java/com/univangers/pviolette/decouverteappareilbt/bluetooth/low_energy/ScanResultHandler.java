package com.univangers.pviolette.decouverteappareilbt.bluetooth.low_energy;

import android.bluetooth.BluetoothDevice;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

public class ScanResultHandler extends Handler{

    BluetoothLeService bluetoothLeService;

    public ScanResultHandler(BluetoothLeService bluetoothLeService) {
        this.bluetoothLeService = bluetoothLeService;
    }

    @Override
    public void handleMessage(Message msg) {
        BluetoothDevice device = msg.getData().getParcelable("device");
        Log.i("handler", device.toString());
        this.bluetoothLeService.addDevice(device);
    }
}
