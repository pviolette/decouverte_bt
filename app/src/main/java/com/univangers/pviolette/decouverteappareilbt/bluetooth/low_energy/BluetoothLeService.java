package com.univangers.pviolette.decouverteappareilbt.bluetooth.low_energy;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.le.BluetoothLeScanner;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.util.Log;

import com.univangers.pviolette.decouverteappareilbt.bluetooth.BluetoothService;
import com.univangers.pviolette.decouverteappareilbt.bluetooth.BluetoothServiceListener;
import com.univangers.pviolette.decouverteappareilbt.bluetooth.BluetoothStateBroadcastReceiver;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class BluetoothLeService implements BluetoothService {

    private BluetoothLeScanner scanner;
    private BtLeScanCallback scanCallback;
    private BluetoothAdapter bluetoothAdapter;

    private BluetoothServiceListener listener;

    private List<BluetoothDevice> listBtDevice;
    private Context context;
    private Set<String> addresses;

    private BroadcastReceiver receiver;

    public BluetoothLeService(BluetoothAdapter bluetoothAdapter, List<BluetoothDevice> listBtDevice, Context context) {
        this.bluetoothAdapter = bluetoothAdapter;
        this.listBtDevice = listBtDevice;
        this.context = context;
        this.addresses = new HashSet<>();
        for (BluetoothDevice device : listBtDevice) {
            this.addresses.add(device.getAddress());
        }

    }

    public void initialize(){
        this.receiver = new BluetoothStateBroadcastReceiver(this);
        IntentFilter filter = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);
        this.context.registerReceiver(this.receiver, filter);
    }

    @Override
    public boolean startDiscovery() {
        if(this.bluetoothAdapter == null || !this.bluetoothAdapter.isEnabled()) return false;

        Log.i("task_frag", "Starting discovery");
        this.scanner = this.bluetoothAdapter.getBluetoothLeScanner();
        this.scanCallback = new BtLeScanCallback(new ScanResultHandler(this));
        this.scanner.startScan(this.scanCallback);

        this.listener.notifyDiscoveryStarted();
        return true;
    }

    @Override
    public boolean cancelDiscovery() {
        if (this.scanCallback != null) {
            this.scanner.stopScan(this.scanCallback);
            this.scanCallback = null;
            this.listener.notifyDiscoveryEnded();
            return true;
        }
        return false;
    }

    protected void addDevice(BluetoothDevice device) {
        if (this.addresses.contains(device.getAddress())) {
            return;
        }

        this.listBtDevice.add(device);
        this.addresses.add(device.getAddress());

        if(this.listener != null){
            this.listener.notifyNewDevice(device);
        }
    }

    @Override
    public List<BluetoothDevice> getListBtDevice() {
        return listBtDevice;
    }

    @Override
    public ArrayList<BluetoothDevice> getArrayListBtDevice() {return (ArrayList<BluetoothDevice>) listBtDevice;}

    public void setListener(BluetoothServiceListener listener) {
        this.listener = listener;
    }

    public void removeListener(){
        this.listener = null;
    }

    public boolean isDiscoveryRunning(){
        return this.scanCallback != null;
    }

    @Override
    public void destroy() {
        this.context.unregisterReceiver(this.receiver);
    }
}
