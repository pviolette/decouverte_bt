package com.univangers.pviolette.decouverteappareilbt.bluetooth.arduino;

import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;

import java.util.List;

/**
 * @author Paulin Violette
 */
public interface ArduinoListener {

    void notifyDeviceConnected();

    void notifyServicesDiscovered(List<BluetoothGattService> bluetoothGattServiceList);

    void notifyDeviceDisconnected();

    void notifyCharacteristicChanged(BluetoothGattCharacteristic characteristic);
}
