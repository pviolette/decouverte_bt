package com.univangers.pviolette.decouverteappareilbt.bluetooth.classic;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import com.univangers.pviolette.decouverteappareilbt.bluetooth.BluetoothService;
import com.univangers.pviolette.decouverteappareilbt.bluetooth.BluetoothServiceListener;
import com.univangers.pviolette.decouverteappareilbt.bluetooth.BluetoothStateBroadcastReceiver;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Bluetooth2Service implements BluetoothService {

    private BluetoothAdapter bluetoothAdapter;
    private Context context;
    private ArrayList<BluetoothDevice> devices;

    private Set<String> addresses;
    private BluetoothServiceListener listener;
    private BroadcastReceiver receiver;
    private BroadcastReceiver receiverBTState;

    public Bluetooth2Service(BluetoothAdapter bluetoothAdapter, Context context, ArrayList<BluetoothDevice> devices) {
        this.bluetoothAdapter = bluetoothAdapter;
        this.context = context;
        this.devices = devices;
        this.addresses = new HashSet<>();
        for (BluetoothDevice device : devices) {
            this.addresses.add(device.getAddress());
        }
    }

    public void initialize(){
        this.receiverBTState = new BluetoothStateBroadcastReceiver(this);
        IntentFilter filter = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);
        this.context.registerReceiver(this.receiverBTState, filter);
    }


    @Override
    public void setListener(BluetoothServiceListener listener) {
        this.listener = listener;
    }

    @Override
    public void removeListener() {
        this.listener = null;
    }

    @Override
    public boolean startDiscovery() {
        //TODO instanciate BroadcastReceiver
        this.receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                    // Discovery has found a device. Get the BluetoothDevice
                    // object and its info from the Intent.
                    BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                    addDevice(device);
                }
            }
        };
        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        context.registerReceiver(receiver, filter);
        this.bluetoothAdapter.startDiscovery();
        return true;
    }

    protected void addDevice(BluetoothDevice device){
        if (this.addresses.contains(device.getAddress())) {
            return;
        }

        this.devices.add(device);
        this.addresses.add(device.getAddress());

        if(this.listener != null){
            this.listener.notifyNewDevice(device);
        }

    }

    @Override
    public boolean cancelDiscovery() {
        this.context.unregisterReceiver(this.receiver);
        this.receiver = null;
        this.bluetoothAdapter.cancelDiscovery();
        return true;
    }

    @Override
    public boolean isDiscoveryRunning() {
        return false;
    }

    @Override
    public List<BluetoothDevice> getListBtDevice() {
        return this.devices;
    }

    @Override
    public ArrayList<BluetoothDevice> getArrayListBtDevice() {
        return this.devices;
    }

    @Override
    public void destroy() {
        this.context.unregisterReceiver(this.receiverBTState);
        if(this.receiver != null){
            this.context.unregisterReceiver(this.receiver);
        }
    }
}
