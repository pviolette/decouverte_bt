package com.univangers.pviolette.decouverteappareilbt.bluetooth.arduino;

import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothProfile;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import java.util.ArrayList;

/**
 * @author Paulin Violette
 */
public class ArduinoGattCallback extends BluetoothGattCallback {


    public static final int WHAT_CONNECTED = 1;
    public static final int WHAT_DISCONNECTED = 2;
    public static final int WHAT_SERVICES_DISCOVERED = 3;
    public static final int WHAT_CHARACTERISTIC_CHANGED = 4;


    public static final String DATA_GATT_SERVICES = "services";
    public static final String DATA_CHARACTERISTIC = "characteristic";

    private Handler handler;
    private ArduinoListener listener;

    public ArduinoGattCallback(ArduinoListener listener) {
        this.listener = listener;
        this.handler = new ArduinoMessageHandler(listener);
    }

    @Override
    public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
        if(newState == BluetoothProfile.STATE_CONNECTED){
            Message message = this.handler.obtainMessage(WHAT_CONNECTED);
            this.handler.sendMessage(message);
        }else if(newState == BluetoothProfile.STATE_DISCONNECTED){
            Message message = this.handler.obtainMessage(WHAT_DISCONNECTED);
            this.handler.sendMessage(message);
        }
    }

    @Override
    public void onServicesDiscovered(final BluetoothGatt gatt, int status) {
        ArrayList<BluetoothGattService> bluetoothGattServices = new ArrayList<>(gatt.getServices());

        Message message = this.handler.obtainMessage(WHAT_SERVICES_DISCOVERED);
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList(DATA_GATT_SERVICES, bluetoothGattServices);
        message.setData(bundle);
        this.handler.sendMessage(message);
    }

    @Override
    public void onCharacteristicChanged(BluetoothGatt gatt, final BluetoothGattCharacteristic characteristic) {
        Log.i("temp", characteristic.toString());
        //Characteristic not Parcelable with API < 24, so we use a runnable
        if(Build.VERSION.SDK_INT >= 24){
            Message message = this.handler.obtainMessage(WHAT_CHARACTERISTIC_CHANGED);
            Bundle bundle = new Bundle();
            bundle.putParcelable(DATA_CHARACTERISTIC, characteristic);
            message.setData(bundle);
            this.handler.sendMessage(message);
        }else{
            this.handler.post(new Runnable() {
                @Override
                public void run() {
                    listener.notifyCharacteristicChanged(characteristic);
                }
            });
        }
    }
}
