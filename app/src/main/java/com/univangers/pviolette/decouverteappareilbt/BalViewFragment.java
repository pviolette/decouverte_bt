package com.univangers.pviolette.decouverteappareilbt;

import android.app.Activity;
import android.app.Fragment;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.univangers.pviolette.decouverteappareilbt.bluetooth.arduino.ArduinoGattCallback;
import com.univangers.pviolette.decouverteappareilbt.bluetooth.arduino.ArduinoListener;
import com.univangers.pviolette.decouverteappareilbt.graph.DataPoint;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

public class BalViewFragment extends Fragment implements ArduinoListener {

    private Context context;
    private BalViewInteractionListener mListener;

    private BluetoothGatt bluetoothGatt;
    private BluetoothGattCharacteristic hm10Characteristic;


    private Button disconnectButton;

    private TextView connectedDeviceAdressTextView;
    private TextView temperatureAverageTextView;
    private TextView maxTemperatureTextView;
    private TextView minTemperatureTextView;
    private TextView currentTemperatureView;
    private TextView lastLetterTimeView;
//    private LineGraphView graphView;

    private int letterCount = 0;
    private TextView hasLettersTextView;
    private Button emptyButton;
    private ImageView letterIconImageView;
    private ImageView weatherIconImageView;


    private LinkedList<DataPoint> temperatures;

    private double minTemperature = 1000; //Will never have something bigger that could be a true min
    private double maxTemperature = -400; //Same, but this time the laws of physics are on our side
    private double temperatureAverage;
    private int temperatureCount;
    private boolean disconnectRequested = false;
    private boolean connected;
    private BluetoothDevice device = null;

    private int lastLetterTime = 0;


    public BalViewFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        boolean connected = false;
        if (savedInstanceState != null) {
            this.connected = savedInstanceState.getBoolean("connected");
            this.minTemperature = savedInstanceState.getDouble("min");
            this.maxTemperature = savedInstanceState.getDouble("max");
            this.temperatureAverage = savedInstanceState.getDouble("avg");
            this.temperatureCount = savedInstanceState.getInt("temp_count");
            DataPoint[] dataPoints = (DataPoint[]) savedInstanceState.getParcelableArray("temp_measurements");
            this.temperatures = new LinkedList<>(Arrays.asList(dataPoints));
            this.letterCount = savedInstanceState.getInt("letter_count");

        }

        if (this.temperatures == null) {
            this.temperatures = new LinkedList<>();
        }
        if (this.connected) {
            this.device = savedInstanceState.getParcelable("connected_device");
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_bal_view, container, false);
//        this.graphView = view.findViewById(R.id.temperature_graph);
//        this.graphView.setDataSet(this.temperatures);
        this.letterIconImageView = view.findViewById(R.id.letters_icons);
        this.weatherIconImageView = view.findViewById(R.id.weather_image);

        this.connectedDeviceAdressTextView = view.findViewById(R.id.bt_connected_device_adress);
        this.connectedDeviceAdressTextView.setText(R.string.not_connected_label);
        this.disconnectButton = view.findViewById(R.id.bt_disconnect_btn);
        this.disconnectButton.setEnabled(false);
        this.disconnectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onDisconnectButtonClick();
            }
        });

        this.temperatureAverageTextView = view.findViewById(R.id.temperature_average);
        this.maxTemperatureTextView = view.findViewById(R.id.max_temperature);
        this.minTemperatureTextView = view.findViewById(R.id.min_temperature);

        this.hasLettersTextView = view.findViewById(R.id.bal_status_text);

        this.emptyButton = view.findViewById(R.id.empty_button);
        this.emptyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (bluetoothGatt != null && hm10Characteristic != null) {
                    hm10Characteristic.setValue("r");
                    bluetoothGatt.writeCharacteristic(hm10Characteristic);
                }
            }
        });

        this.updateHasLettersStatus(this.letterCount);

        this.currentTemperatureView = view.findViewById(R.id.current_temperature);

        this.lastLetterTimeView = view.findViewById(R.id.last_letter_time);

        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.context = activity;
        Log.i("attach", "attch");
        if (context instanceof BalViewInteractionListener) {
            mListener = (BalViewInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }

        if (this.device != null) {
            this.connectToDevice(this.device);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
        Log.i("attach", "attch");
        if (context instanceof BalViewInteractionListener) {
            mListener = (BalViewInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }

        if (this.device != null) {
            this.connectToDevice(this.device);
        }
    }

    public void connectToDevice(BluetoothDevice device) {
        if (this.bluetoothGatt != null) {
            this.bluetoothGatt.disconnect();
            this.bluetoothGatt.close();
            this.bluetoothGatt = null;
        }

        this.bluetoothGatt = device.connectGatt(this.context, true, new ArduinoGattCallback(this));

        Toast.makeText(this.context, "Connecting to " + device.getAddress(), Toast.LENGTH_SHORT).show();
        this.connectedDeviceAdressTextView.setText(String.format("Connecting to %s (%s)", device.getName(), device.getAddress()));
//        this.setViewToConnectedMode();
    }

    public boolean isConnected() {
        return this.bluetoothGatt != null;
    }

    public void disconnect() {
        if (this.bluetoothGatt == null) return;
        this.bluetoothGatt.disconnect();
        this.bluetoothGatt.close();
        this.bluetoothGatt = null;
    }

    @Override
    public void notifyDeviceConnected() {
        Toast.makeText(this.context, "Connected", Toast.LENGTH_SHORT).show();

        this.disconnectButton.setEnabled(true);

        Toast.makeText(this.context, "Connected to " + this.bluetoothGatt.getDevice().getAddress(), Toast.LENGTH_SHORT).show();

        final BluetoothDevice device = this.bluetoothGatt.getDevice();
        this.connectedDeviceAdressTextView.setText(String.format("%s (%s)", device.getName(), device.getAddress()));
        this.bluetoothGatt.discoverServices();
    }

    @Override
    public void notifyDeviceDisconnected() {
        Toast.makeText(this.context, "Disconnected", Toast.LENGTH_LONG).show();

        if (this.disconnectRequested) {
            this.bluetoothGatt.close();
            this.bluetoothGatt = null;
            this.disconnectButton.setEnabled(false);
            this.connectedDeviceAdressTextView.setText(R.string.not_connected_label);
            this.disconnectRequested = false;
            this.setViewToNotConnectedMode();
        } else {
            final BluetoothDevice device = this.bluetoothGatt.getDevice();
            this.connectedDeviceAdressTextView.setText(String.format(Locale.getDefault(), "%s (%s) - Lost connection", device.getName(), device.getAddress()));
        }
    }

    private void setViewToNotConnectedMode() {

    }

    @Override
    public void notifyServicesDiscovered(List<BluetoothGattService> bluetoothGattServiceList) {
        for (BluetoothGattService service : bluetoothGattServiceList) {
            for (BluetoothGattCharacteristic characteristic : service.getCharacteristics()) {
                if (characteristic.getUuid().toString().equals("0000ffe1-0000-1000-8000-00805f9b34fb")) {
                    this.hm10Characteristic = characteristic;
                    this.bluetoothGatt.setCharacteristicNotification(characteristic, true);

                    this.hm10Characteristic.setValue("?");
                    this.bluetoothGatt.writeCharacteristic(this.hm10Characteristic);
                }
            }
        }
    }

    @Override
    public void notifyCharacteristicChanged(BluetoothGattCharacteristic characteristic) {
        Log.i("characteristic", characteristic.getStringValue(0));
        String stringValue = characteristic.getStringValue(0).replace("\r\n", "");
        char type = stringValue.charAt(0);
        Log.i("characteristic", "type=" + type + ", value='" + stringValue + "'");
        switch (type) {
            case 'C':
                this.updateTemperature(stringValue.substring(1));
                break;
            case 'o':
                try {
                    int letterCount = Integer.parseInt(stringValue.substring(1));
                    this.updateHasLettersStatus(letterCount);
                } catch (NumberFormatException e) {
                    Log.e("characteristic", "Error characteristic with string value : '" + stringValue + "'", e);
                }
                break;
            case 'L':
                this.updateLastLetterTime(stringValue.substring(1));
                break;
            case 'n':
                this.updateHasLettersStatus(0);
                break;
        }

    }

    private void updateLastLetterTime(String substring) {
        try {
            this.lastLetterTime = Integer.parseInt(substring);

            String label = this.getResources().getQuantityString(R.plurals.last_letter_time_label, this.lastLetterTime, this.lastLetterTime);
            this.lastLetterTimeView.setText(label);

        } catch (NumberFormatException exception) {
            Log.e("timer", "Erreur message reçu", exception);
        }
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        this.context = null;
    }

    public void onDisconnectButtonClick() {
        this.disconnectRequested = true;
        this.bluetoothGatt.disconnect();
    }

    protected void updateMinTemperature(double newValue) {
        Log.d("temp", "Update min temp to " + newValue);
        this.minTemperature = newValue;
        this.minTemperatureTextView.setText(String.valueOf(newValue));
    }

    protected void updateMaxTemperature(double newValue) {
        Log.d("temp", "Update max temp to " + newValue);
        this.maxTemperature = newValue;
        this.maxTemperatureTextView.setText(String.valueOf(newValue));
    }

    protected void updateTemperature(String stringValue) {
        try {
            double value = Double.valueOf(stringValue);
            this.currentTemperatureView.setText(String.format(Locale.getDefault(), "%.02f °C", value));
            this.temperatures.addLast(new DataPoint("", value));
            if(value > 20){
                this.weatherIconImageView.setImageResource(R.drawable.ic_sun);
            } else if(value > 10) {
                this.weatherIconImageView.setImageResource(R.drawable.ic_cloud);
            } else {
                this.weatherIconImageView.setImageResource(R.drawable.ic_snow);
            }
            if (this.temperatures.size() > 20) {
                this.temperatures.removeFirst();
            }
//            this.graphView.setDataSet(this.temperatures);

            if (value < this.minTemperature) {
                this.updateMinTemperature(value);
            }
            if (value > this.maxTemperature) {
                this.updateMaxTemperature(value);
            }

            this.temperatureAverage = (this.temperatureAverage * temperatureCount + value) / (this.temperatureCount + 1);
            this.temperatureCount++;

            this.temperatureAverageTextView.setText(String.format(Locale.getDefault(), "%.02f", this.temperatureAverage));

            Log.i("temp", "Value : " + value);
        } catch (NumberFormatException exception) {
            Log.e("temp", "Erreur dans le message reçu", exception);
        }
    }


    protected void updateHasLettersStatus(int letterCount) {
        this.letterCount = letterCount;
        String label;
        if (letterCount == 0) {
            label = this.getResources().getString(R.string.no_letters_label);
            this.letterIconImageView.setImageResource(R.drawable.ic_mail_outline_black_24dp);
        } else if (letterCount == 255) {
            label = this.getResources().getString(R.string.overflow_letters_label);
            this.letterIconImageView.setImageResource(R.drawable.ic_mail_black_24dp);
        } else {
            label = this.getResources().getQuantityString(R.plurals.has_letters_label, this.letterCount, letterCount);
            this.letterIconImageView.setImageResource(R.drawable.ic_mail_black_24dp);
        }
        this.hasLettersTextView.setText(label);
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface BalViewInteractionListener {
        // TODO: Update argument type and name
        void onDisconnect(Uri uri);
    }
}
