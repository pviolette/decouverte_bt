package com.univangers.pviolette.decouverteappareilbt.bluetooth.low_energy;

import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanResult;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import java.util.List;

public class BtLeScanCallback extends ScanCallback {


    private Handler handler;

    public BtLeScanCallback(Handler handler) {

        this.handler = handler;
    }

    @Override
    public void onScanResult(int callbackType, ScanResult result) {

        Log.i("scan_result", result.getDevice().toString());
        if(result.getDevice() != null) {
            Message message = this.handler.obtainMessage();
            Bundle bundle = new Bundle();
            bundle.putParcelable("device", result.getDevice());
            message.setData(bundle);
            this.handler.sendMessage(message);
        }
    }

    @Override
    public void onBatchScanResults(List<ScanResult> results) {
        for(ScanResult result : results){
            Log.i("scan_result", result.getDevice().toString());
            if(result.getDevice() != null) {
                Message message = this.handler.obtainMessage();
                Bundle bundle = new Bundle();
                bundle.putParcelable("device", result.getDevice());
                message.setData(bundle);
                this.handler.sendMessage(message);
            }
        }
    }

    @Override
    public void onScanFailed(int errorCode) {
        super.onScanFailed(errorCode);
    }
}
