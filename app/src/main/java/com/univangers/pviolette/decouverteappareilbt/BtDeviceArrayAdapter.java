package com.univangers.pviolette.decouverteappareilbt;

import android.bluetooth.BluetoothClass;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

public class BtDeviceArrayAdapter extends ArrayAdapter<BluetoothDevice> {

    public BtDeviceArrayAdapter(@NonNull Context context, @NonNull List<BluetoothDevice> objects) {
        super(context, 0, objects);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        BluetoothDevice device = this.getItem(position);

        View view = convertView;
        if(view == null){
            view = LayoutInflater.from(this.getContext()).inflate(R.layout.bluetooth_device_list_item, parent, false);
        }

        final TextView nameTextView = view.findViewById(R.id.bt_device_item_name);
        nameTextView.setText(device.getName());

        switch (device.getBluetoothClass().getMajorDeviceClass()){
            case BluetoothClass.Device.Major.AUDIO_VIDEO:

                break;
            case BluetoothClass.Device.Major.HEALTH:

                break;
        }

        final TextView addressTextView = view.findViewById(R.id.bt_device_adress);
        addressTextView.setText(device.getAddress());

        return view;
    }
}
