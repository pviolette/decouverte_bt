package com.univangers.pviolette.decouverteappareilbt.bluetooth.arduino;

import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import java.util.ArrayList;

/**
 * @author Paulin Violette
 */
public class ArduinoMessageHandler extends Handler {

    private ArduinoListener listener;

    public ArduinoMessageHandler(ArduinoListener listener) {
        this.listener = listener;
    }

    @Override
    public void handleMessage(Message msg) {
        switch (msg.what){
            case ArduinoGattCallback.WHAT_CONNECTED:
                this.listener.notifyDeviceConnected();
                break;
            case ArduinoGattCallback.WHAT_DISCONNECTED:
                this.listener.notifyDeviceDisconnected();
                break;
            case ArduinoGattCallback.WHAT_SERVICES_DISCOVERED:
                ArrayList<BluetoothGattService> bluetoothGattServices = msg.getData().getParcelableArrayList(ArduinoGattCallback.DATA_GATT_SERVICES);
                this.listener.notifyServicesDiscovered(bluetoothGattServices);
                break;
            case ArduinoGattCallback.WHAT_CHARACTERISTIC_CHANGED:
                BluetoothGattCharacteristic characteristic = msg.getData().getParcelable(ArduinoGattCallback.DATA_CHARACTERISTIC);
                this.listener.notifyCharacteristicChanged(characteristic);
                break;
        }
    }
}
