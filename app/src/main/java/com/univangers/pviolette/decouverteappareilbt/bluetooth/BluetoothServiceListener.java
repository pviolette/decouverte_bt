package com.univangers.pviolette.decouverteappareilbt.bluetooth;

import android.bluetooth.BluetoothDevice;

public interface BluetoothServiceListener {

    void notifyNewDevice(BluetoothDevice device);
    void notifyDiscoveryStarted();
    void notifyDiscoveryEnded();
}
