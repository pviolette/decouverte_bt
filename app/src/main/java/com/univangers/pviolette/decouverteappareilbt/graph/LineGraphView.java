package com.univangers.pviolette.decouverteappareilbt.graph;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

import com.univangers.pviolette.decouverteappareilbt.R;

import java.util.Iterator;
import java.util.List;
import java.util.Locale;

/**
 * @author Paulin Violette
 * Vue pour un graphe.
 *
 * Pourrais être refactoré en une vue composite, avec une vue pour les labels de chaque axes et une vue pour le graphe
 */
public class LineGraphView extends View{

    private int dotColor;
    private int yLabelColor;

    private List<DataPoint> dataSet;

    private Paint linePaint;
    private Paint labelPaint;
    private Paint axePaint;
    private Paint dotPaint;
    private Paint gridPaint;

    Float preferredMin = null;
    Float preferredMax = null;

    Integer maxDataPoint = null;

    private String yLabelFormatString = "%.02f";

    public LineGraphView(Context context) {
        super(context);
        initPaints(Color.BLACK, Color.BLACK, Color.RED, Color.BLUE, Color.LTGRAY);
    }

    public LineGraphView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        int lineColor = Color.BLACK,
                xLabelColor = Color.BLACK,
                yLabelColor = Color.RED,
                dotColor = Color.BLUE,
                gridColor = Color.LTGRAY;

        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.LineGraphView);
        try{
            if(typedArray.hasValue(R.styleable.LineGraphView_preferred_min)){
                this.preferredMin = typedArray.getFloat(R.styleable.LineGraphView_preferred_min, 0);
            }
            if(typedArray.hasValue(R.styleable.LineGraphView_preferred_max)){
                this.preferredMax = typedArray.getFloat(R.styleable.LineGraphView_preferred_max, 0);
            }
            if(typedArray.hasValue(R.styleable.LineGraphView_max_data_points)){
                this.maxDataPoint = typedArray.getInt(R.styleable.LineGraphView_max_data_points, 0);
            }
            if(typedArray.hasValue(R.styleable.LineGraphView_y_label_format_string)){
                this.yLabelFormatString = typedArray.getString(R.styleable.LineGraphView_y_label_format_string);
            }

            lineColor = typedArray.getColor(R.styleable.LineGraphView_color_line, lineColor);
            yLabelColor = typedArray.getColor(R.styleable.LineGraphView_color_y_label, yLabelColor);
            dotColor = typedArray.getColor(R.styleable.LineGraphView_color_dot, dotColor);
            gridColor = typedArray.getColor(R.styleable.LineGraphView_color_grid, gridColor);
        }finally {
            typedArray.recycle();
        }

        this.initPaints(lineColor, xLabelColor, yLabelColor, dotColor, gridColor);
    }

    protected void initPaints(int lineColor, int xLabelColor, int yLabelColor, int dotColor, int gridColor){
        this.linePaint = new Paint();
        this.linePaint.setStrokeWidth(3);
        this.linePaint.setColor(lineColor);

        this.labelPaint =new Paint();
        this.labelPaint.setTextSize(10.0f);
        this.labelPaint.setColor(xLabelColor);

        this.axePaint = new Paint();
        this.axePaint.setColor(yLabelColor);
        this.axePaint.setStrokeWidth(3);
        this.axePaint.setTextSize(10.0f);

        this.dotPaint = new Paint();
        this.dotPaint.setColor(dotColor);

        this.gridPaint = new Paint();
        this.gridPaint.setColor(gridColor);
    }

    public void setDataSet(List<DataPoint> dataSet){
        this.dataSet = dataSet;
        this.invalidate();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        if(this.dataSet == null || this.dataSet.isEmpty()){
            return;
        }

        int width = this.getWidth();
        int height = this.getHeight();

        int labelHeight = 50; //TODO not arbitrary
        int graphHeight  = height - labelHeight;


        float maxYValue = (float) dataSet.get(0).y;
        float minYValue = (float) dataSet.get(0).y;
        for (DataPoint dataPoint: this.dataSet) {
            if (maxYValue < dataPoint.y) {
                maxYValue = (float) dataPoint.y;
            }
            if(minYValue > dataPoint.y){
                minYValue = (float) dataPoint.y;
            }
        }



        float graphMin = minYValue;
        if(this.preferredMin != null){
            graphMin = graphMin < preferredMin ? graphMin : preferredMin;
        }
        graphMin = (float) Math.floor(graphMin);
        float graphMax = maxYValue; // TODO parameter to set this
        if(this.preferredMax != null){
            graphMax = graphMax > preferredMax ? graphMax : preferredMax;
        }
        graphMax = (float) Math.ceil(graphMax);

        if(graphMin == graphMax){
            graphMax++;
            graphMin--;
        }

        this.drawGraph(canvas, graphMin, graphMax, width - 20, graphHeight - 20, 20, 20);
    }

    protected void drawGraph(Canvas canvas, float graphMin, float graphMax, float graphWidth, float graphHeight, float deltaX, float deltaY){
        float graphDelta = Math.abs(graphMax - graphMin);

        boolean labelUp = true;

        float stepX;

        if(this.maxDataPoint == null){
            stepX = graphWidth / this.dataSet.size();
        }else{
            stepX = graphWidth / this.maxDataPoint;
        }

        float x = 0;

        this.drawGrid(canvas, graphMin, graphMax, graphWidth, graphHeight, deltaX, deltaY);

        Iterator<DataPoint> it = this.dataSet.iterator();
        DataPoint current = it.next();

        DataPoint next;
        while(it.hasNext()){
            next = it.next();

            float labelSize = this.labelPaint.measureText(current.xLabel);
            float nextLabelSize = this.labelPaint.measureText(next.xLabel);

            //Draw line
            float y0 = graphHeight - (float) ((current.y - graphMin) / graphDelta * graphHeight);
            float y1 = graphHeight - (float) ((next.y - graphMin) / graphDelta * graphHeight);

            canvas.drawLine(deltaX + x + labelSize / 2, deltaY + y0, deltaX + x + stepX + nextLabelSize / 2, deltaY + y1, this.linePaint);
            canvas.drawCircle(deltaX + x, deltaY + y0, 5, this.dotPaint);


            //Draw label
            canvas.drawText(current.xLabel, deltaX + x, deltaY + graphHeight + (labelUp ? 25 : 50), this.labelPaint);

            x+= stepX;
            labelUp = !labelUp;
            current = next;
        }

        //Draw last label;
        canvas.drawText(current.xLabel, deltaX + x, deltaY + graphHeight + (labelUp ? 25 : 50), this.labelPaint);
        canvas.drawCircle(deltaX + x, deltaY + graphHeight - (float) ((current.y - graphMin) / graphDelta * graphHeight), 5, this.dotPaint);


        float yZeroAxes = graphHeight - ((- graphMin) / graphDelta * graphHeight);
        canvas.drawLine(deltaX, deltaY + yZeroAxes, graphWidth, deltaY + yZeroAxes, this.axePaint);
        canvas.drawText("0°C", 0, deltaY + yZeroAxes, this.axePaint);

        canvas.drawText(String.format(Locale.getDefault(),this.yLabelFormatString, graphMax), 0, deltaY, this.axePaint);

        if(graphMin != 0){
            canvas.drawText(String.format(Locale.getDefault(), this.yLabelFormatString, graphMin), 0, deltaY + graphHeight, this.axePaint);
        }
    }

    protected void drawGrid(Canvas canvas, float graphMin, float graphMax,float graphWidth, float graphHeight, float deltaX, float deltaY){
        int diff = (int) (graphMax - graphMin);

        float stepY = graphHeight / diff;
        float y = deltaY;
        for(int i = 0; i <= diff; ++i){
            canvas.drawLine(deltaX, y, graphWidth, y, this.gridPaint);
            if(i != 0 && i != diff && (graphMax - i) % 5 == 0){
                canvas.drawText(String.format(Locale.getDefault(), this.yLabelFormatString, graphMax - i), 0, y, this.axePaint);
            }
            y+= stepY;
        }
    }
}
