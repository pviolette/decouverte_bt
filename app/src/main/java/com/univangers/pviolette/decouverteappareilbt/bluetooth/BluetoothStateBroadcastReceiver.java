package com.univangers.pviolette.decouverteappareilbt.bluetooth;

import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

public class BluetoothStateBroadcastReceiver extends BroadcastReceiver {

    private BluetoothService bluetoothService;

    public BluetoothStateBroadcastReceiver(BluetoothService bluetoothService) {
        this.bluetoothService = bluetoothService;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (BluetoothAdapter.ACTION_STATE_CHANGED.equals(action)) {
            int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, -1);
            if (state == BluetoothAdapter.STATE_OFF) {
                Log.i("BT", "State Turning off");
                this.bluetoothService.cancelDiscovery();
                Toast.makeText(context, "Bluetooth is off. Turn it on to allow discovery", Toast.LENGTH_SHORT).show();
            } else if (state == BluetoothAdapter.STATE_ON) {
                Log.i("BT", "State Turning on");
                Toast.makeText(context, "Bluetooth on. Starting discovery", Toast.LENGTH_SHORT).show();
                this.bluetoothService.startDiscovery();
            }
        }
    }
}
