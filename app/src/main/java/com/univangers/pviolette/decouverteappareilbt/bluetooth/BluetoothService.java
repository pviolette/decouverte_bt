package com.univangers.pviolette.decouverteappareilbt.bluetooth;

import android.bluetooth.BluetoothDevice;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Paulin Violette
 */
public interface BluetoothService {

    void setListener(BluetoothServiceListener listener);
    void removeListener();

    boolean startDiscovery();
    boolean cancelDiscovery();
    boolean isDiscoveryRunning();

    void initialize();
    void destroy();

    List<BluetoothDevice> getListBtDevice();
    ArrayList<BluetoothDevice> getArrayListBtDevice();

}
