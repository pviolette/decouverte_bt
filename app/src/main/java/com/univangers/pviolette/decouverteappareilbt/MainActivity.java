package com.univangers.pviolette.decouverteappareilbt;

import android.Manifest;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.univangers.pviolette.decouverteappareilbt.bluetooth.BluetoothService;
import com.univangers.pviolette.decouverteappareilbt.bluetooth.BluetoothServiceListener;
import com.univangers.pviolette.decouverteappareilbt.bluetooth.low_energy.BluetoothLeService;

import java.util.ArrayList;

/*
 * Fonctionnalités implémentées :
 * MAQUETTE CASIER A
  * * Lecture Temperature
  * * Qté courrier
  * * Temps depuis le dernier courrier reçu.
  *
  * Reste à faire :
  *  * Refacto partie decouverte dans fragment
  *  * Changement layout portrait : uniquement partie decouverte, au click sur un elt de la liste lancement d'une nouvel activité
  *  * Amélioration design interface
 */
public class MainActivity extends Activity implements BluetoothServiceListener, AdapterView.OnItemClickListener, BalViewFragment.BalViewInteractionListener {

    private static final int REQUEST_ENABLE_BT = 1;

    private BtDeviceArrayAdapter listAdapter;


    private BluetoothAdapter bluetoothAdapter;
    private BluetoothService bluetoothService;

    private Button startButton;
    private Button cancelButton;

    private BalViewFragment fragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ArrayList<BluetoothDevice> listBtDevice = null;
        if (savedInstanceState != null) {
            listBtDevice = savedInstanceState.getParcelableArrayList("devices");

        }
        if (listBtDevice == null) {
            listBtDevice = new ArrayList<>();
        }

        int MY_PERMISSIONS_REQUEST = 200;
        int permissions= ContextCompat.checkSelfPermission (this, Manifest.permission.ACCESS_FINE_LOCATION);
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                MY_PERMISSIONS_REQUEST);

        boolean btAvailable = true;
        this.bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (this.bluetoothAdapter == null) {
            Toast.makeText(this, "This device does not support bluetooth", Toast.LENGTH_LONG).show();
            btAvailable = false;
        } else {
            this.bluetoothService = new BluetoothLeService(this.bluetoothAdapter, listBtDevice, this);
            this.bluetoothService.setListener(this);
            this.bluetoothService.initialize();
        }

        this.listAdapter = new BtDeviceArrayAdapter(this, listBtDevice);

        ListView listViewBtDevice = this.findViewById(R.id.list_bt_device);
        listViewBtDevice.setAdapter(this.listAdapter);
        listViewBtDevice.setOnItemClickListener(this);

        this.startButton = this.findViewById(R.id.start_discovery);
        startButton.setEnabled(btAvailable);
        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                discoverBluetoothDevices();
            }
        });

        this.cancelButton = this.findViewById(R.id.cancel_discovery);
        cancelButton.setEnabled(false);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancelDiscovery();
            }
        });

        this.fragment = (BalViewFragment) this.getFragmentManager().findFragmentById(R.id.bal_view_fragment);

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {

        outState.putParcelableArrayList("devices", this.bluetoothService.getArrayListBtDevice());
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onDestroy() {
        this.bluetoothService.destroy();
        super.onDestroy();
    }

    //----------------------------------------------------------------------------------------------
    // Discovery related methods
    //

    protected void discoverBluetoothDevices() {

        if (!this.bluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
            Log.i("main_activity", "Bluetooth is not enabled");
        } else {
            this.bluetoothService.startDiscovery();
        }
    }

    protected void cancelDiscovery() {
        this.bluetoothService.cancelDiscovery();
    }

    @Override
    public void notifyNewDevice(BluetoothDevice device) {
        Log.i("bt", "New device " + device.toString());
        this.listAdapter.notifyDataSetChanged();
    }

    @Override
    public void notifyDiscoveryStarted() {
        this.setDiscoveryButtonState(true);
    }

    @Override
    public void notifyDiscoveryEnded() {
        this.setDiscoveryButtonState(false);
    }

    protected void setDiscoveryButtonState(boolean discovering) {
        Log.d("Button_state", "Button state: " + discovering);
        this.startButton.setEnabled(!discovering);
        this.cancelButton.setEnabled(discovering);
    }

    //----------------------------------------------------------------------------------------------
    // Device connection related methods
    //

    //Method called when user touch an bluetooth device in the list
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        BluetoothDevice device = this.listAdapter.getItem(position);
        if(device == null){
            Log.i("wtf", "wtf");
            return;
        }

        Log.i("BT_connect", "Selected " + position + " : " + device.toString());

        this.connectToDevice(device);
    }

    protected void connectToDevice(BluetoothDevice device){
        this.fragment.connectToDevice(device);
    }

    @Override
    public void onDisconnect(Uri uri) {

    }
}
