package com.univangers.pviolette.decouverteappareilbt.graph;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * @author Paulin Violette
 * Point de donnée pour un graphe. Composé d'un label et une valeur
 *
 * Immutable.
 */
public class DataPoint implements Parcelable{

    final String xLabel;
    final double y;

    public DataPoint(String xLabel, double y) {
        this.xLabel = xLabel;
        this.y = y;
    }

    private DataPoint(Parcel in) {
        xLabel = in.readString();
        y = in.readDouble();
    }

    public static final Creator<DataPoint> CREATOR = new Creator<DataPoint>() {
        @Override
        public DataPoint createFromParcel(Parcel in) {
            return new DataPoint(in);
        }

        @Override
        public DataPoint[] newArray(int size) {
            return new DataPoint[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeString(xLabel);
        dest.writeDouble(y);
    }
}
